# Travelling? Use a VPN to Get Free Spotify Abroad #

Spotify is one of the best music apps around, but if you have tried to use the free version abroad, you will have noticed that it stops working. Instead, it asks you to sign up for the premium option if you would like to continue using it on your travels. If you are a paid member already, this is fine, but if a few advertisements do not bother you and you would rather have the free product, this is an issue.
The good news is that there is an easy workaround. If you have a quality VPN, all you need to do is connect your device to a server in your home country. 

Spotify will then believe you are still at home and continue to allow you to listen to some enjoyable free music on your device. 
Many of you will already have a VPN anyway, as it is also an excellent piece of privacy software. It allows you to browse the internet in privacy. Trackers, hackers, agencies, and governments cannot keep tabs on what you are doing. Furthermore, a VPN gives you an extra layer of security, especially when using unsecured public WiFi hotspots that you will find at the airport or your hotel.
So, the next time you are on your travels and want to listen to Spotify, just remember this tip. It could save you a few dollars and the hassle of signing up for a premium account. 

[https://getmoreprivacy.com](https://getmoreprivacy.com)